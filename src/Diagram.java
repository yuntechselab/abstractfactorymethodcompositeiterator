import java.util.ArrayList;
import java.util.Iterator;

public class Diagram extends Uml_Object {
	ArrayList<Uml_Object> array = new ArrayList<>();
	protected String name = "Diagram";

	public void addObject(Uml_Object uml_Object) {
		array.add(uml_Object);
	}

	@Override
	public void draw() {
		// TODO Auto-generated method stub
		System.out.println("I'm Diagram");
		Iterator<Uml_Object> iterator = array.iterator();
		while(iterator.hasNext()){
			iterator.next().draw();
		}
	}

}
