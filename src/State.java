
public abstract class State extends Uml_Object{
	public abstract void draw();
	protected String name = "State";
}
