public abstract class Transition extends Uml_Object {
	public abstract void draw();

	protected String name = "Transition";

}
