
public class Uml1_Factory implements Factory{

	@Override
	public State createState(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Transition createTransition(String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uml_Object createUmlObejct(String type) {
		// TODO Auto-generated method stub
		Uml_Object uml_Object;
        switch (type.toLowerCase())
        {
            case "state":
            	uml_Object = new Uml1_State();
                break;
            case "transition":
            	uml_Object =  new Uml1_Transition();
                break;
            case "diagram":
            	uml_Object =  new Diagram();
                break;
            default: throw new IllegalArgumentException("No such object.");
        }
 
      
        return uml_Object;	}

	

}
