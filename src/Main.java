
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Factory uml1_factory = new Uml1_Factory();

		Uml_Object uml1_state = uml1_factory.createUmlObejct("state");
		Uml_Object uml1_transition = uml1_factory.createUmlObejct("transition");
		Uml_Object uml1_diagram = uml1_factory.createUmlObejct("diagram");
		
		uml1_diagram.addObject(uml1_state);
		uml1_diagram.addObject(uml1_transition);
		
		uml1_diagram.draw();
		
		
		
	}

}
